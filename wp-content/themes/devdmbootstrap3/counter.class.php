<?php

class Counter {

    private $counter_path;
    private $count;

    public function __construct($configurator) {
        $this->counter_path = get_template_directory() . '/templates/' . $configurator . '/counter.txt';
    }

    private function load_counter() {
        if (!file_exists($this->counter_path)) {
            $this->reset_counter();
        } else {
            $this->read_counter();
        }
    }

    private function reset_counter() {
        $handle = fopen($this->counter_path, 'w') or die('Cannot open file: counter.txt');
        fwrite($handle, '0');
        fclose($handle);

        $this->count = 0;
    }

    private function read_counter() {
        $handle = fopen($this->counter_path, 'r');
        $data = fread($handle, filesize($this->counter_path));
        $this->count = $data;
    }

    public function getCount() {
        $this->load_counter();
        return $this->count + 1;
    }

    public function increaseCount() {
        $this->load_counter();
        $handle = fopen($this->counter_path, 'w') or die('Cannot open file: counter.txt');
        fwrite($handle, $this->count + 1);
        fclose($handle);
    }

}

?>