<?php get_header(); ?>
<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>
<!-- start content container -->
<div class="row dmbs-content">

    <?php //left sidebar ?>
    <?php get_sidebar('left'); ?>

    <div class="col-md-<?php devdmbootstrap3_main_content_width(); ?> dmbs-main">

        <?php
        //if this was a search we display a page header with the results count. If there were no results we display the search form.
        if (is_search()) :

            $total_results = $wp_query->found_posts;

            echo "<h2 class='page-header'>" . sprintf(__('%s Search Results for "%s"', 'devdmbootstrap3'), $total_results, get_search_query()) . "</h2>";

            if ($total_results == 0) :
                get_search_form(true);
            endif;

        endif;
        ?>

        <?php
        // theloop
        if (have_posts()) : while (have_posts()) : the_post();

                // single post
                if (is_single()) :
                    ?>

                    <div <?php post_class(); ?>>

                        <!-- <h2 class="page-header"><?php //the_title(); ?></h2> --> 
                        <?php get_template_part('template-part', 'konfigurator'); ?>

                    </div>
                    <?php
                // list of posts
                else :
                    $post   = get_post();
                    ?>
                    <div <?php post_class(); ?>>
                        <h2 class="page-header">
                            <div class="text-right dblock">
                                <a class="fleft" href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf(__('Permalink to %s', 'devdmbootstrap3'), the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php the_title(); ?></a>
                                <a download="<?php echo $post->post_name; ?>.csv" class="download-list btn btn-default" href="<?php echo (get_template_directory_uri() . '/templates/' . $post->post_name . '/' . 'list.csv') ?>">Download List</a>
                                <button class="clear-list btn btn-default" data-configurator="<?php echo $post->post_name; ?>">Clear List</button>
                                <button class="backup-list btn btn-default" data-configurator="<?php echo $post->post_name; ?>">Backup</button>
                                <a class="download_backups btn btn-default" href="<?php echo (get_template_directory_uri() . '/templates/' . $post->post_name . '/backups/' . 'backups.php') ?>">Download Backups</a>
<!--                                <button class="reset-counter btn btn-default" data-configurator="<?php //echo $post->post_name; ?>">Reset Counter</button>-->
                            </div>
                        </h2>

                        <?php if (has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail(); ?>
                            <div class="clear"></div>
                        <?php endif; ?>
                        <?php the_content(); ?>
                        <?php wp_link_pages(); ?>
                        <?php get_template_part('template-part', 'postmeta'); ?>
                        <?php if (comments_open()) : ?>
                            <div class="clear"></div>
                            <p class="text-right">
                                <a class="btn btn-success" href="<?php the_permalink(); ?>#comments"><?php comments_number(__('Leave a Comment', 'devdmbootstrap3'), __('One Comment', 'devdmbootstrap3'), '%' . __(' Comments', 'devdmbootstrap3')); ?> <span class="glyphicon glyphicon-comment"></span></a>
                            </p>
                        <?php endif; ?>
                    </div>

                <?php endif; ?>

            <?php endwhile; ?>
            <?php posts_nav_link(); ?>
        <?php else: ?>

            <?php get_404_template(); ?>

        <?php endif; ?>

    </div>

    <?php //get the right sidebar ?>

</div>
<!-- end content container -->

<?php get_footer(); ?>

