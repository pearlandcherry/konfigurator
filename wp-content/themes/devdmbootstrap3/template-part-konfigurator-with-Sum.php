<?php
if (!empty($_POST) && isset($_POST['pre_summarise']) && $_POST['pre_summarise'] != ''):
    $post = get_post();

    // START Reading from template
    $template = get_field('template');
    $template_path = explode("/wp-content/", $template['url']);
    $file_path = WP_CONTENT_DIR . '/' . $template_path[1];
    if (!file_exists($file_path)) {
        die("The template " . $template['title'] . " does not exist");
    }
    $template_csv = file_get_contents($file_path, true);
    // END Reading from template
    // START Replacing values
    foreach ($_POST as $key => $value) {
        $temp_val = is_array($value) ? implode(', ', $value) : $value;

		$temp_val = preg_replace('/[^A-Za-z0-9\. @]/', '', $temp_val);

        $template_csv = str_replace('{' . substr($key, 4) . '}', $temp_val, $template_csv);
    }
    // END Replacing values
    // START Adding record on list    
    //Check if folder template existst
    $abs_path = get_template_directory() . '/templates/' . $post->post_name . '/';
    if (!file_exists($abs_path)) {
        // START Creating follder templates
        mkdir($abs_path, 0777, true);
        // END Creating follder templates
    }

    $list_path = $abs_path . 'list.csv';
    //Check if file List.cs existst
    if (!file_exists($list_path)) {
        // START Reading from headers
        $headers = get_field('headers');
        $headers_url = explode("/wp-content/", $headers['url']);
        $headers_path = WP_CONTENT_DIR . '/' . $headers_url[1];
        if ($headers == '' || !file_exists($headers_path)) {
            die("The headers file does not exist");
        }
        $headers_content = file_get_contents($headers_path, true);
        // END Reading from headers
        // START Creating File List.csv
        $handle = fopen($list_path, 'w') or die('Cannot open file: list.csv');
        fwrite($handle, $headers_content);
        fclose($handle);
        // END Creating File List.csv
    }

    //article_no_prefix
    //SET Article No.
//    include('counter.class.php');
//    $counter = new Counter($post->post_name);
//    $article_no = strval($counter->getCount());
//    $article_no = get_field("article_no_prefix") . $article_no;    
//    $template_csv = str_replace('{article_no}', $article_no, $template_csv);    
    $handle = fopen($list_path, 'a') or die('Cannot open file: list.csv');
    $data = "\n" . $template_csv;
    fwrite($handle, $data);
    fclose($handle);
    //$counter->increaseCount();
    ?>
    <div class="row">
        <div class="col-xs-12">
            <h3><?php echo get_field('thank_you_msg') ?></h3>
        </div>
    </div>
	<!-- Google Analytics -->
    <script> 
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-7879514-1', 'auto');  // Replace with your property ID.
    ga('send', 'pageview');

    </script>
    <!-- End Google Analytics -->
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            ga('send', {
                    hitType: 'event',				
                    eventCategory: 'Konfigurator',
                    eventAction: 'success',
                    eventLabel: 'Garage'				
            });
        });
    </script>
    <?php
else:
    ?>
    <script type="text/javascript">
        var defined_slider_count = 0;
        var tiefe = [];
        var uniqueTiefe = [];
        var hohe = [];
        var uniqueHohe = [];
        var filtered_combination = [];
        var selected_breite = 0;
        var selected_tiefe = 0;
        var selected_hohe = 0;
        var selected_type = 0;
        var data_price = 0;
        function update_countables() {
            jQuery('form > .row > div:not(.deny_pricable) .priceable').each(function () {
                jQuery(this).attr('data-value', (parseFloat(jQuery(this).attr('data-price')) + parseFloat(jQuery(this).attr('data-montage_preis') != undefined ? jQuery(this).attr('data-montage_preis') : 0)) * parseFloat(jQuery(this).attr('data-count')));
            });
        }
        function calculate_price() {
            update_countables();
            var data_value = 0;
            var data_percentage = jQuery('#pre_percentage').val();
            jQuery('form > .row > div:not(.deny_pricable) .priceable').each(function () {
                data_value += parseFloat(jQuery(this).attr('data-value'));
            });
            data_value = data_value - (data_value * data_percentage / 100);

            jQuery('#summarise').text(data_value.toFixed(2));
            jQuery('#pre_summarise').val(data_value.toFixed(2));
            update_generals();
        }
        function update_generals() {
            general_anzahl_code = ' ';
            general_breite_code = 0;
            general_tiefe_code = 0;
            general_hohe_code = 0;
            defined_slider_count = -1;
            selected_article_no = '';
            selected_article_price = '';
            selected_montage_preis = '';
            first_einzelbox_count = 1;
            doublebox_count = 1;
            jQuery('.the_type').each(function () {
                if (jQuery(this).closest('.deny_pricable').length === 0) {
                    general_anzahl_code = jQuery(' option:selected', this).text();
                    selected_type = jQuery(' option:selected', this).data('position');
                    defined_slider_count = jQuery(this).data('defined_slider_count');
                    first_einzelbox = jQuery(' option:selected', this).data('first_einzelbox');
                    first_einzelbox_count = parseFloat(jQuery(this).closest('.priceable').attr('data-count'));
                }
            });
            jQuery('.the_breite').each(function () {
                if (jQuery(this).closest('.deny_pricable').length === 0) {
                    general_breite_code = parseFloat(jQuery(this).val());
                }
            });
            jQuery('.the_tiefe').each(function () {
                if (jQuery(this).closest('.deny_pricable').length === 0) {
                    general_tiefe_code = parseFloat(jQuery(this).val());
                }
            });
            jQuery('.the_hohe').each(function () {
                if (jQuery(this).closest('.deny_pricable').length === 0) {
                    general_hohe_code = parseFloat(jQuery(this).val());
                }
            });
            if (defined_slider_count > -1) {
                filtered_combination = window['dimensions_' + defined_slider_count].filter(function (item) {
                    return (item[0] === general_breite_code && item[1] === general_tiefe_code && item[2] === general_hohe_code);
                });
                if (filtered_combination.length > 0) {
                    selected_article_no = filtered_combination[0][6];
                    if (selected_type === 5) {
                        selected_article_no = filtered_combination[0][7];
                    } else if (selected_type === 4) {
                        selected_article_no = '';
                        doublebox_count = 2;
                    }

                    if (first_einzelbox) {
                        selected_article_price = (first_einzelbox_count - 1) * filtered_combination[0][selected_type] + filtered_combination[0][3];
                    } else {
                        selected_article_price = filtered_combination[0][selected_type];
                    }
                    selected_montage_preis = filtered_combination[0][8] * first_einzelbox_count * doublebox_count;
                }
            }
            jQuery('#general_anzahl_code').val(general_anzahl_code);
            jQuery('#general_breite_code').val(general_breite_code);
            jQuery('#general_tiefe_code').val(general_tiefe_code);
            jQuery('#general_hohe_code').val(general_hohe_code);

            jQuery('#general_config_article_no_code').val(selected_article_no);
            jQuery('#general_config_price_code').val(selected_article_price);
            jQuery('#general_config_montage_price_code').val(selected_montage_preis);
        }
        function set_price(obj) {
            selected_breite = jQuery('#' + jQuery(obj).data('breite_code')).val();
            if (typeof selected_breite != 'undefined') {
                selected_tiefe = jQuery('#' + jQuery(obj).data('tiefe_code')).val();
                selected_hohe = jQuery('#' + jQuery(obj).data('hohe_code')).val();
                selected_type = jQuery('#' + jQuery(obj).data('type_code') + ' option:selected').data('position');
                first_einzelbox = jQuery('#' + jQuery(obj).data('type_code') + ' option:selected').data('first_einzelbox');
                first_einzelbox_count = 1;
                doublebox_count = 1;
                filtered_combination = window['dimensions_' + jQuery(obj).data('defined_slider_count')].filter(function (item) {
                    return (item[0] == selected_breite && item[1] == selected_tiefe && item[2] == selected_hohe);
                });
                if (filtered_combination[0]) {
                    if (first_einzelbox) {
                        first_einzelbox_count = parseFloat(jQuery('#' + jQuery(obj).data('type_code')).closest('.priceable').attr('data-count'));
                        first_einzelbox_value = (first_einzelbox_count - 1) * filtered_combination[0][selected_type] + filtered_combination[0][3];
                        selected_article_price = first_einzelbox_value;
                        jQuery('#' + jQuery(obj).data('type_code')).closest('.priceable').attr('data-price', first_einzelbox_value / first_einzelbox_count);
                    } else {
                        selected_article_price = filtered_combination[0][selected_type];
                        jQuery('#' + jQuery(obj).data('type_code')).closest('.priceable').attr('data-price', filtered_combination[0][selected_type]);
                    }
                    selected_article_no = filtered_combination[0][6];
                    if (selected_type === 5) {
                        selected_article_no = filtered_combination[0][7];
                    } else if (selected_type === 4) {
                        selected_article_no = '';
                        doublebox_count = 2;
                    }
                    selected_montage_preis = filtered_combination[0][8] * first_einzelbox_count * doublebox_count;

                    jQuery('#' + jQuery(obj).data('article_no_code')).val(selected_article_no);
                    jQuery('#' + jQuery(obj).data('article_price_code')).val(selected_article_price);
                    jQuery('#' + jQuery(obj).data('montage_preis_code')).val(selected_montage_preis);
                    jQuery('#' + jQuery(obj).data('type_code')).closest('.priceable').attr('data-montage_preis', selected_montage_preis);
                }
                jQuery('.calculate_measure_pm2').attr('data-option-measure', parseFloat(selected_breite) * parseFloat(selected_tiefe));
                jQuery('.calculate_measure_limit').attr('data-option-measure', parseFloat(selected_breite));
            }
            update_price();
            calculate_price();
        }
        function update_price() {
            data_price = 0;
            montage_prise = 0;
            calculate_measure = '';
            if (jQuery('.calculate_measure_pm2').length > 0) {
                calculate_measure = 'pm2';
            }
            if (jQuery('.calculate_measure_limit').length > 0) {
                calculate_measure = 'limit';
            }
            jQuery('.calculate_measure_' + calculate_measure).closest('.priceable').find('input.individual').each(function () {
                jQuery(this).val('');
            });
            jQuery('.calculate_measure_' + calculate_measure).closest('.priceable').find('input:checked').each(function () {
                option_type = jQuery(this).data('option_type');
                option_price = parseFloat(jQuery(this).data('option-price'));
                option_calculate = jQuery(this).data('option-calculate');
                the_data_count = parseFloat(jQuery(this).closest('.priceable').attr('data-count'));
                individual_price = 0;
                individual_montage = 0;                        
                switch (option_type) {
                    case 'pm2':
                        {
                            option_measure = parseFloat(jQuery(this).attr('data-option-measure'));
                            individual_price = option_calculate == 'once' ? (option_price * option_measure / the_data_count) : (option_price * option_measure);
                            data_price += individual_price;
                        }
                        break;
                    case 'limit':
                        {
                            option_measure = parseFloat(jQuery(this).attr('data-option-measure'));
                            option_limit = parseFloat(jQuery(this).data('option-limit'));
                            option_price_1 = parseFloat(jQuery(this).data('option-price_1'));
                            option_price_2 = parseFloat(jQuery(this).data('option-price_2'));
                            
                            individual_price = option_calculate == 'once' ? (((option_measure <= option_limit ? option_price_1 : option_price_2)) / the_data_count) : ((option_measure <= option_limit ? option_price_1 : option_price_2));
                            data_price += individual_price;
                        }
                        break;
                    case 'fix':
                    {
                        individual_price = option_calculate == 'once' ? (option_price / the_data_count) : (option_price);
                        data_price += individual_price;
                    }
                        break;
                    default:{
                        individual_price = option_calculate == 'once' ? (option_price / the_data_count) : (option_price)
                        data_price += individual_price;
                    }
                }
                individual_montage = parseFloat(jQuery(this).data('montage_preis')) * the_data_count;
                montage_prise += individual_montage;
                
                jQuery('.individual_'+jQuery(this).attr('id')+'_price').val((individual_price * the_data_count).toFixed(2));
                jQuery('.individual_'+jQuery(this).attr('id')+'_montage').val(individual_montage.toFixed(2));
                jQuery('.individual_'+jQuery(this).attr('id')+'_article').val(jQuery(this).data('article_no'));
            });
            jQuery('.calculate_measure_' + calculate_measure).closest('.priceable').attr('data-price', data_price);
            jQuery('.calculate_measure_' + calculate_measure).closest('.priceable').find('.print_price').val(data_price);
            jQuery('.calculate_measure_' + calculate_measure).closest('.priceable').attr('data-montage_preis', montage_prise);
            jQuery('.calculate_measure_' + calculate_measure).closest('.priceable').find('.preis_code').val(montage_prise);
            calculate_price();
        }
        function breite_change(obj) {
            filtered_items = window['dimensions_' + jQuery(obj).data('defined_slider_count')].filter(function (item) {
                return item[0] == jQuery(obj).val();
            });
            tiefe = [];
            filtered_items.forEach(function (element) {
                tiefe.push(element[1]);
            });
            uniqueTiefe = tiefe.filter(function (value, index, self) {
                return self.indexOf(value) === index;
            });
            jQuery('#' + jQuery(obj).data('tiefe_code') + ' option').each(function () {
                if (uniqueTiefe.indexOf(parseFloat(jQuery(this).val())) > -1) {
                    jQuery(this).attr('class', 'show_options');
                } else {
                    jQuery(this).attr('class', 'hide_options');
                }
            });
            if (jQuery('#' + jQuery(obj).data('tiefe_code') + ' option:selected').hasClass('hide_options')) {
                jQuery('#' + jQuery(obj).data('tiefe_code')).val(uniqueTiefe[0]);
            } else {
                tiefe_change('#' + jQuery(obj).data('tiefe_code'));
            }
            jQuery('#' + jQuery(obj).data('tiefe_code') + ' option.hide_options').hide().attr('class', '');
            jQuery('#' + jQuery(obj).data('tiefe_code') + ' option.show_options').show().attr('class', '');
        }
        function tiefe_change(obj) {
            selected_breite = jQuery('#' + jQuery(obj).data('breite_code')).val();
            filtered_items = window['dimensions_' + jQuery(obj).data('defined_slider_count')].filter(function (item) {
                return (item[0] == selected_breite && item[1] == jQuery(obj).val());
            });
            hohe = [];
            filtered_items.forEach(function (element) {
                hohe.push(element[2]);
            });
            uniqueHohe = hohe.filter(function (value, index, self) {
                return self.indexOf(value) === index;
            });
            jQuery('#' + jQuery(obj).data('hohe_code') + ' option').each(function () {
                if (uniqueHohe.indexOf(parseFloat(jQuery(this).val())) > -1) {
                    jQuery(this).attr('class', 'show_options');
                } else {
                    jQuery(this).attr('class', 'hide_options');
                }
            });
            if (jQuery('#' + jQuery(obj).data('hohe_code') + ' option:selected').hasClass('hide_options')) {
                jQuery('#' + jQuery(obj).data('hohe_code')).val(uniqueHohe[0]);
            }
            jQuery('#' + jQuery(obj).data('hohe_code') + ' option.hide_options').hide().attr('class', '');
            jQuery('#' + jQuery(obj).data('hohe_code') + ' option.show_options').show().attr('class', '');
        }
        function children_view(parent, view) {
            var parent_code = parent;
            var control_type = jQuery('#' + parent).data('control_type');
            parent_option = jQuery('#' + parent).parent().find('option:selected').data('option_code');
            jQuery('.child_of_' + parent_code).each(function () {
                var display_on = jQuery(this).data('display_on');
                var display_on_arr = display_on.split(', ');
                if (display_on_arr.indexOf(parent_option + "") > -1 && view) {
                    jQuery(this).addClass('child_to_show');
                } else {
                    jQuery(this).addClass('child_to_hide');
                }
            });
            jQuery('.child_of_' + parent_code).each(function () {
                control_type = jQuery(this).find('[data-control_type="defined_slider"]').length > 0 ? 'defined_slider' : (jQuery(this).find('[data-control_type="select"]').length > 0 ? 'select' : (jQuery(this).find('[data-control_type="radio"]').length > 0 || jQuery(this).find('[data-control_type="radio_image"]').length > 0 || jQuery(this).find('[data-control_type="checkbox"]').length > 0 ? 'radio' : (jQuery(this).parent('[data-control_type="select"]').length > 0 ? 'select_option' : '')));
                if (control_type == 'select') {
                    if (jQuery(this).hasClass('child_to_hide')) {
                        jQuery('select', this).val(jQuery('select', this).find("option:first").val());
                    }
                    if (jQuery(this).find('.counter_field').length > 0) {
                        var counter_value = jQuery(this).find('.counter_field').val();
                        var counter_of = jQuery(this).find('.counter_field').data('counter-of');
                        var counter_of_arr = counter_of.split(', ');
                        counter_of_arr.forEach(function (parent_code) {
                            jQuery('#' + parent_code).closest('.priceable').attr('data-count', counter_value);
                            set_price('#' + parent_code);
                        });
                    }
                    ;
                } else if (control_type == 'select_option') {
                    if (jQuery(this).hasClass('child_to_hide')) {
                        jQuery(this).removeAttr('selected');
                        jQuery(this).parent().val(jQuery(this).parent().find("option.to_show:not(.to_hide):first").val());
                    }
                } else if (control_type == 'radio') {
                    if (jQuery(this).hasClass('child_to_hide')) {
                        jQuery('input', this).removeAttr('checked');
                        jQuery(this).closest('.radio-inline').removeClass('active');
                    }
                } else {
                    if (jQuery(this).hasClass('child_to_hide')) {
                        jQuery('input[type!=hidden]', this).val('');
                        jQuery('textarea', this).val('');
                        var slider_min_val = jQuery('input', this).data('slider-min');
                        if (typeof slider_min_val != 'undefined') {
                            var slider_max_val = jQuery('input', this).data('slider-max');
                            var slider_step = jQuery('input', this).data('my-step');
                            jQuery('.slider', this).slider({min: slider_min_val, max: slider_max_val, step: slider_step, value: slider_min_val});
                        }
                    }
                }
            });
            jQuery('.child_to_show').attr('style', 'display: inline-block !important').removeClass('child_to_show').removeClass('deny_pricable');
            jQuery('.child_to_hide').attr('style', 'display: none !important').removeClass('child_to_hide').addClass('deny_pricable');
        }
    </script>
    <div class="row">
        <div class="col-xs-12">
            <form method="post" name="generator_form">
                <input type="hidden" name="pre_summarise" id="pre_summarise" value="0"/>
                <input type="hidden" name="pre_percentage" id="pre_percentage" value="<?php the_field('percentage'); ?>"/>

                <input type="hidden" name="pre_<?php the_field('general_anzahl_code'); ?>" id="general_anzahl_code" value=""/>
                <input type="hidden" name="pre_<?php the_field('general_breite_code'); ?>" id="general_breite_code" value=""/>
                <input type="hidden" name="pre_<?php the_field('general_tiefe_code'); ?>" id="general_tiefe_code" value=""/>
                <input type="hidden" name="pre_<?php the_field('general_hohe_code'); ?>" id="general_hohe_code" value=""/>
                <input type="hidden" name="pre_<?php the_field('general_config_article_no_code'); ?>" id="general_config_article_no_code" value=""/>
                <input type="hidden" name="pre_<?php the_field('general_config_price_code'); ?>" id="general_config_price_code" value=""/>
                <input type="hidden" name="pre_<?php the_field('general_config_montage_price_code'); ?>" id="general_config_montage_price_code" value=""/>
                <?php
                if (have_rows('group')):
                    while (have_rows('group')): the_row();
                        ?>
                        <div class="row fs0">
                            <div class="col-xs-12 dinblock fnone vtop">
                                <h2><?php the_sub_field('group_name'); ?></h2>
                            </div>
                            <?php
                            if (have_rows('fields')):
                                while (have_rows('fields')): the_row();
                                    ?>
                                    <div 
                                        data-display_on="<?php the_sub_field('display_on_option_code'); ?>" 
                                        class="<?php echo get_sub_field('is_child') ? 'child_of_' . get_sub_field('parent_code') : ''; ?> <?php echo implode(' ', get_sub_field('width')); ?> <?php echo (get_sub_field('offset') ? implode(' ', get_sub_field('offset')) : ''); ?> col-xs-12 dinblock fnone vtop">
                                            <?php if (get_row_layout() == 'text'): ?>
                                            <<?php the_sub_field('type'); ?>><?php the_sub_field('text'); ?></<?php the_sub_field('type'); ?>>
                                        <?php elseif (get_row_layout() == 'line'): ?> 
                                            <hr/>
                                        <?php elseif (get_row_layout() == 'input_text'): ?> 
                                            <label for="<?php the_sub_field('code'); ?>"><?php the_sub_field('name'); ?></label>
                                            <div class="form-group">
                                                <input type="text" id="<?php the_sub_field('code'); ?>" name="pre_<?php the_sub_field('code'); ?>" <?php echo get_sub_field('f_required') ? 'required' : ''; ?>/>
                                                <?php if (get_sub_field('has_article_no')): ?>                                                
                                                    <input id="<?php the_sub_field('article_no_code'); ?>" type="hidden" name="pre_<?php the_sub_field('article_no_code'); ?>" value="<?php the_sub_field('article_no'); ?>"/>
                                                <?php endif; ?>
                                            </div>
                                        <?php elseif (get_row_layout() == 'text_area'): ?> 
                                            <label for="<?php the_sub_field('code'); ?>"><?php the_sub_field('name'); ?></label>
                                            <div class="form-group">
                                                <textarea id="<?php the_sub_field('code'); ?>" name="pre_<?php the_sub_field('code'); ?>" <?php echo get_sub_field('f_required') ? 'required' : ''; ?>></textarea>
                                            </div>
                                        <?php elseif (get_row_layout() == 'number'): ?> 
                                            <label for="<?php the_sub_field('code'); ?>"><?php the_sub_field('name'); ?></label>
                                            <div class="form-group">
                                                <input type="number" id="<?php the_sub_field('code'); ?>" name="pre_<?php the_sub_field('code'); ?>" min="<?php the_sub_field('min'); ?>" max="<?php the_sub_field('max'); ?>" <?php echo get_sub_field('f_required') ? 'required' : ''; ?>/>
                                            </div>
                                        <?php elseif (get_row_layout() == 'email'): ?> 
                                            <label for="<?php the_sub_field('code'); ?>"><?php the_sub_field('name'); ?></label>
                                            <div class="form-group">
                                                <input type="email" id="<?php the_sub_field('code'); ?>" name="pre_<?php the_sub_field('code'); ?>" <?php echo get_sub_field('f_required') ? 'required' : ''; ?>/>
                                            </div>
                                        <?php elseif (get_row_layout() == 'date'): ?> 
                                            <label for="<?php the_sub_field('code'); ?>"><?php the_sub_field('name'); ?></label>
                                            <div class="form-group">
                                                <div class='input-group date' id='date_<?php the_sub_field('code'); ?>'>
                                                    <input type="text" id="<?php the_sub_field('code'); ?>" name="pre_<?php the_sub_field('code'); ?>" <?php echo get_sub_field('f_required') ? 'required' : ''; ?>/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                jQuery(document).ready(function ($) {
                                                    $("#date_" + '<?php the_sub_field('code'); ?>').datetimepicker({format: 'DD/MM/YYYY'});
                                                });
                                            </script>
                                        <?php elseif (get_row_layout() == 'time'): ?> 
                                            <label for="<?php the_sub_field('code'); ?>"><?php the_sub_field('name'); ?></label>
                                            <div class="form-group">
                                                <div class='input-group date' id='time_<?php the_sub_field('code'); ?>'>
                                                    <input type="text" id="<?php the_sub_field('code'); ?>" name="pre_<?php the_sub_field('code'); ?>" <?php echo get_sub_field('f_required') ? 'required' : ''; ?>/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                jQuery(document).ready(function ($) {
                                                    $("#time_" + '<?php the_sub_field('code'); ?>').datetimepicker({format: 'HH:mm:ss'});
                                                });
                                            </script>
                                        <?php elseif (get_row_layout() == 'slider'): ?> 
                                            <label for="<?php the_sub_field('code'); ?>"><?php the_sub_field('name'); ?></label>
                                            <div class="form-group">
                                                <input name="pre_<?php the_sub_field('code'); ?>" id="<?php the_sub_field('code'); ?>" data-slider-id='<?php the_sub_field('code'); ?>' type="text" data-slider-min="<?php the_sub_field('min'); ?>" data-slider-max="<?php the_sub_field('max'); ?>" data-my-step="<?php the_sub_field('step'); ?>" <?php echo get_sub_field('f_required') ? 'required' : ''; ?>/>
                                            </div>
                                            <script type="text/javascript">
                                                jQuery(document).ready(function ($) {
                                                    $('#<?php the_sub_field('code'); ?>').slider({
                                                        step: '<?php the_sub_field('step'); ?>'
                                                    });
                                                });
                                            </script>
                                        <?php elseif (get_row_layout() == 'defined_slider'): ?> 
                                            <?php
                                            $the_parent_code = get_sub_field('parent_code');
                                            $type_title = get_sub_field('type_title');
                                            $breite_title = get_sub_field('breite_title');
                                            $tiefe_title = get_sub_field('tiefe_title');
                                            $hohe_title = get_sub_field('hohe_title');
                                            $type_code = get_sub_field('type_code');
                                            $breite_code = get_sub_field('breite_code');
                                            $tiefe_code = get_sub_field('tiefe_code');
                                            $hohe_code = get_sub_field('hohe_code');
                                            $einzelbox_title = get_sub_field('einzelbox_title');
                                            $doppelbox_title = get_sub_field('doppelbox_title');
                                            $anbaubox_title = get_sub_field('anbaubox_title');
                                            $einzelbox_code = get_sub_field('einzelbox_code');
                                            $article_no_code = get_sub_field('article_no_code');
                                            $article_price_code = get_sub_field('article_price_code');
                                            $montage_preis_code = get_sub_field('montage_preis_code');
                                            $doppelbox_code = get_sub_field('doppelbox_code');
                                            $anbaubox_code = get_sub_field('anbaubox_code');
                                            $first_einzelbox = get_sub_field('anbaubox_with_first_einzelbox');
                                            $is_parent = get_sub_field('is_parent');
                                            if (have_rows('options')):
                                                while (have_rows('options')): the_row();
                                                    if (have_rows('option_list')):
                                                        $index = 0;
                                                        ?>
                                                        <script type="text/javascript">
                                                            var dimensions = [];
                                                            defined_slider_count++;
                                                            window['dimensions_' + defined_slider_count] = [];
                                                        </script>
                                                        <?php
                                                        $breite = array();
                                                        $tiefe = array();
                                                        $hohe = array();
                                                        $einzelbox = array();
                                                        $doppelbox = array();
                                                        $anbaubox = array();
                                                        while (have_rows('option_list')): the_row();
                                                            array_push($breite, floatval(get_sub_field('option_breite')));
                                                            array_push($tiefe, floatval(get_sub_field('option_tiefe')));
                                                            array_push($hohe, floatval(get_sub_field('option_hohe')));
                                                            array_push($einzelbox, floatval(get_sub_field('option_einzelbox')));
                                                            array_push($doppelbox, floatval(get_sub_field('option_doppelbox')));
                                                            array_push($anbaubox, floatval(get_sub_field('option_anbaubox')));
                                                            ?>
                                                            <script type = "text/javascript" >
                                                                window['dimensions_' + defined_slider_count].push([]);
                                                                window['dimensions_' + defined_slider_count][<?php echo $index; ?>].push(<?php the_sub_field('option_breite') ?>);
                                                                window['dimensions_' + defined_slider_count][<?php echo $index; ?>].push(<?php the_sub_field('option_tiefe') ?>);
                                                                window['dimensions_' + defined_slider_count][<?php echo $index; ?>].push(<?php the_sub_field('option_hohe') ?>);
                                                                window['dimensions_' + defined_slider_count][<?php echo $index; ?>].push(<?php the_sub_field('option_einzelbox') ?>);
                                                                window['dimensions_' + defined_slider_count][<?php echo $index; ?>].push(<?php the_sub_field('option_doppelbox') ?>);
                                                                window['dimensions_' + defined_slider_count][<?php echo $index; ?>].push(<?php the_sub_field('option_anbaubox') ?>);
                                                                window['dimensions_' + defined_slider_count][<?php echo $index; ?>].push('<?php the_sub_field('einzelbox_article_no') ?>');
                                                                window['dimensions_' + defined_slider_count][<?php echo $index; ?>].push('<?php the_sub_field('anbaubox_article_no') ?>');
                                                                window['dimensions_' + defined_slider_count][<?php echo $index; ?>].push('<?php the_sub_field('montage_preis') ?>');
                                                            </script>
                                                            <?php
                                                            $index ++;
                                                        endwhile;
                                                        $unique_breite = array_unique($breite);
                                                        $unique_tiefe = array_unique($tiefe);
                                                        $unique_hohe = array_unique($hohe);
                                                        $unique_einzelbox = array_unique($einzelbox);
                                                        $unique_doppelbox = array_unique($doppelbox);
                                                        $unique_anbaubox = array_unique($anbaubox);
                                                        ?>
                                                        <div class="row priceable" data-price="0" data-count="1" data-control_type="defined_slider">
                                                            <div class="col-sm-3 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="<?php echo $type_code; ?>"><?php echo $type_title; ?></label>
                                                                    <select 
                                                                        class="form-control the_type <?php echo $is_parent ? 'parent' : ''; ?>" 
                                                                        id="<?php echo $type_code; ?>"
                                                                        name="pre_<?php echo $type_code; ?>"
                                                                        data-parent_code="<?php echo $is_parent ? $type_code : ''; ?>" 
                                                                        data-control_type="<?php echo $is_parent ? 'select' : ''; ?>">
                                                                            <?php if (count($unique_einzelbox) > 1 || $unique_einzelbox[0] != 0): ?>
                                                                            <option data-position="3" value="Einzelbox" data-option_code="<?php echo $einzelbox_code; ?>"><?php echo $einzelbox_title; ?></option>
                                                                        <?php endif; ?>
                                                                        <?php if (count($unique_doppelbox) > 1 || $unique_doppelbox[0] != 0): ?>
                                                                            <option data-position="4" value="Doppelbox" data-option_code="<?php echo $doppelbox_code; ?>"><?php echo $doppelbox_title; ?></option>
                                                                        <?php endif; ?>
                                                                        <?php if (count($unique_anbaubox) > 1 || $unique_anbaubox[0] != 0): ?>
                                                                            <option data-first_einzelbox="<?php echo $first_einzelbox; ?>" data-position="5" value="Anbaubox" data-option_code="<?php echo $anbaubox_code; ?>"><?php echo $anbaubox_title; ?></option>
                                                                        <?php endif; ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="<?php echo $breite_code; ?>"><?php echo $breite_title; ?></label>
                                                                    <select 
                                                                        class="form-control the_breite parent"
                                                                        id="<?php echo $breite_code; ?>"
                                                                        data-parent_code="<?php echo $breite_code; ?>" 
                                                                        data-control_type="select"
                                                                        name="pre_<?php echo $breite_code; ?>">
                                                                            <?php
                                                                            foreach ($unique_breite as $key => $value):
                                                                                ?>
                                                                            <option data-option_code="<?php echo $value; ?>" value="<?php echo $value; ?>"><?php echo number_format($value, 2, '.', ''); ?></option>
                                                                            <?php
                                                                        endforeach
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="<?php echo $tiefe_code; ?>"><?php echo $tiefe_title; ?></label>
                                                                    <select 
                                                                        class="form-control the_tiefe"
                                                                        id="<?php echo $tiefe_code; ?>"
                                                                        name="pre_<?php echo $tiefe_code; ?>">
                                                                            <?php
                                                                            foreach ($unique_tiefe as $key => $value):
                                                                                ?>
                                                                            <option value="<?php echo $value; ?>"><?php echo number_format($value, 2, '.', ''); ?></option>
                                                                            <?php
                                                                        endforeach
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="<?php echo $hohe_code; ?>"><?php echo $hohe_title; ?></label>
                                                                    <select 
                                                                        class="form-control the_hohe"
                                                                        id="<?php echo $hohe_code; ?>"
                                                                        name="pre_<?php echo $hohe_code; ?>">
                                                                            <?php
                                                                            foreach ($unique_hohe as $key => $value):
                                                                                ?>
                                                                            <option value="<?php echo $value; ?>"><?php echo number_format($value, 2, '.', ''); ?></option>
                                                                            <?php
                                                                        endforeach
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <input id="<?php echo $article_no_code; ?>" type="hidden" name="pre_<?php echo $article_no_code; ?>" value=""/>
                                                            <?php if ($article_price_code): ?>
                                                                <input id="<?php echo $article_price_code; ?>" type="hidden" name="pre_<?php echo $article_price_code; ?>" value="0"/>
                                                            <?php endif; ?>                                                            
                                                            <?php if ($montage_preis_code): ?>
                                                                <input id="<?php echo $montage_preis_code; ?>" type="hidden" name="pre_<?php echo $montage_preis_code; ?>" value="0"/>
                                                            <?php endif; ?>                                                            
                                                        </div>
                                                        <?php
                                                    endif;
                                                endwhile;
                                            endif;
                                            ?>
                                            <script type="text/javascript">
                                                selected_breite = window['dimensions_' + defined_slider_count][0][0];
                                                selected_tiefe = window['dimensions_' + defined_slider_count][0][1];
                                                selected_hohe = window['dimensions_' + defined_slider_count][0][2];
                                                selected_type = 3;

                                                selected_article_no = window['dimensions_' + defined_slider_count][0][6];
                                                selected_article_price = window['dimensions_' + defined_slider_count][0][3];
                                                selected_montage_preis = window['dimensions_' + defined_slider_count][0][8];

                                                filtered_combination = window['dimensions_' + defined_slider_count].filter(function (item) {
                                                    return (item[0] == selected_breite && item[1] == selected_tiefe && item[2] == selected_hohe);
                                                });
                                                jQuery('#' + '<?php echo $type_code; ?>').closest('.priceable').attr('data-price', filtered_combination[0][selected_type]);
                                                jQuery('#' + '<?php echo $type_code; ?>').closest('.priceable').attr('data-montage_preis', selected_montage_preis);

                                                jQuery('#' + '<?php echo $breite_code; ?>').data('defined_slider_count', defined_slider_count);
                                                jQuery('#' + '<?php echo $breite_code; ?>').data('breite_code', '<?php echo $breite_code; ?>');
                                                jQuery('#' + '<?php echo $breite_code; ?>').data('tiefe_code', '<?php echo $tiefe_code; ?>');
                                                jQuery('#' + '<?php echo $breite_code; ?>').data('hohe_code', '<?php echo $hohe_code; ?>');
                                                jQuery('#' + '<?php echo $breite_code; ?>').data('type_code', '<?php echo $type_code; ?>');
                                                jQuery('#' + '<?php echo $breite_code; ?>').data('article_no_code', '<?php echo $article_no_code; ?>');
                                                jQuery('#' + '<?php echo $breite_code; ?>').data('article_price_code', '<?php echo $article_price_code; ?>');
                                                jQuery('#' + '<?php echo $breite_code; ?>').data('montage_preis_code', '<?php echo $montage_preis_code; ?>');
                                                jQuery('#' + '<?php echo $breite_code; ?>').change(function () {
                                                    breite_change('#' + '<?php echo $breite_code; ?>');
                                                    set_price('#' + '<?php echo $breite_code; ?>');
                                                });
                                                jQuery('#' + '<?php echo $tiefe_code; ?>').data('defined_slider_count', defined_slider_count);
                                                jQuery('#' + '<?php echo $tiefe_code; ?>').data('breite_code', '<?php echo $breite_code; ?>');
                                                jQuery('#' + '<?php echo $tiefe_code; ?>').data('tiefe_code', '<?php echo $tiefe_code; ?>');
                                                jQuery('#' + '<?php echo $tiefe_code; ?>').data('hohe_code', '<?php echo $hohe_code; ?>');
                                                jQuery('#' + '<?php echo $tiefe_code; ?>').data('type_code', '<?php echo $type_code; ?>');
                                                jQuery('#' + '<?php echo $tiefe_code; ?>').data('article_no_code', '<?php echo $article_no_code; ?>');
                                                jQuery('#' + '<?php echo $tiefe_code; ?>').data('article_price_code', '<?php echo $article_price_code; ?>');
                                                jQuery('#' + '<?php echo $tiefe_code; ?>').data('montage_preis_code', '<?php echo $montage_preis_code; ?>');
                                                jQuery('#' + '<?php echo $tiefe_code; ?>').change(function () {
                                                    tiefe_change('#' + '<?php echo $tiefe_code; ?>');
                                                    set_price('#' + '<?php echo $breite_code; ?>');
                                                });
                                                jQuery('#' + '<?php echo $hohe_code; ?>').data('defined_slider_count', defined_slider_count);
                                                jQuery('#' + '<?php echo $hohe_code; ?>').data('breite_code', '<?php echo $breite_code; ?>');
                                                jQuery('#' + '<?php echo $hohe_code; ?>').data('tiefe_code', '<?php echo $tiefe_code; ?>');
                                                jQuery('#' + '<?php echo $hohe_code; ?>').data('hohe_code', '<?php echo $hohe_code; ?>');
                                                jQuery('#' + '<?php echo $hohe_code; ?>').data('type_code', '<?php echo $type_code; ?>');
                                                jQuery('#' + '<?php echo $hohe_code; ?>').data('article_no_code', '<?php echo $article_no_code; ?>');
                                                jQuery('#' + '<?php echo $hohe_code; ?>').data('article_price_code', '<?php echo $article_price_code; ?>');
                                                jQuery('#' + '<?php echo $hohe_code; ?>').data('montage_preis_code', '<?php echo $montage_preis_code; ?>');
                                                jQuery('#' + '<?php echo $hohe_code; ?>').change(function () {
                                                    set_price('#' + '<?php echo $breite_code; ?>');
                                                });
                                                jQuery('#' + '<?php echo $type_code; ?>').data('defined_slider_count', defined_slider_count);
                                                jQuery('#' + '<?php echo $type_code; ?>').data('breite_code', '<?php echo $breite_code; ?>');
                                                jQuery('#' + '<?php echo $type_code; ?>').data('tiefe_code', '<?php echo $tiefe_code; ?>');
                                                jQuery('#' + '<?php echo $type_code; ?>').data('hohe_code', '<?php echo $hohe_code; ?>');
                                                jQuery('#' + '<?php echo $type_code; ?>').data('type_code', '<?php echo $type_code; ?>');
                                                jQuery('#' + '<?php echo $type_code; ?>').data('article_no_code', '<?php echo $article_no_code; ?>');
                                                jQuery('#' + '<?php echo $type_code; ?>').data('article_price_code', '<?php echo $article_price_code; ?>');
                                                jQuery('#' + '<?php echo $type_code; ?>').data('montage_preis_code', '<?php echo $montage_preis_code; ?>');
                                                jQuery('#' + '<?php echo $type_code; ?>').change(function () {
                                                    set_price('#' + '<?php echo $breite_code; ?>');
                                                });

                                                jQuery('#' + '<?php echo $article_no_code; ?>').val(selected_article_no);
                                                jQuery('#' + '<?php echo $article_price_code; ?>').val(selected_article_price);
                                                jQuery('#' + '<?php echo $montage_preis_code; ?>').val(selected_montage_preis);

                                                breite_change('#' + '<?php echo $breite_code; ?>');

                                            </script>
                                        <?php elseif (get_row_layout() == 'dropdown'): ?> 
                                            <div class="form-group">
                                                <label for="<?php the_sub_field('code'); ?>"><?php the_sub_field('name'); ?></label>
                                                <select 
                                                <?php echo get_sub_field('is_counter') ? 'data-counter-of="' . get_sub_field('counter_of') . '"' : ''; ?>
                                                    class="form-control <?php echo get_sub_field('is_calculator_type') ? 'calculator_type' : ''; ?> <?php echo get_sub_field('is_counter') ? 'counter_field' : ''; ?> <?php echo get_sub_field('is_parent') ? 'parent' : ''; ?> <?php echo get_sub_field('priceable') ? 'priceable' : ''; ?> <?php echo get_sub_field('priceable') ? 'update_price' : ''; ?>" 
                                                    id="<?php the_sub_field('code'); ?>" 
                                                    name="pre_<?php the_sub_field('code'); ?>" 
                                                    data-parent_code="<?php the_sub_field('code'); ?>" 
                                                    data-control_type="select" 
                                                    data-child-of="<?php echo get_sub_field('parent_code'); ?>"
                                                    data-count="1">
                                                    <script type="text/javascript">
                                                        var slider_prices = [];
                                                    </script>
                                                    <?php
                                                    $priceable = get_sub_field('priceable');
                                                    if (have_rows('options')):
                                                        while (have_rows('options')): the_row();
                                                            $are_children = get_sub_field('are_children');
                                                            if (have_rows('option_list')):
                                                                while (have_rows('option_list')): the_row();
                                                                    ?>
                                                                    <script type="text/javascript">
                                                                        slider_prices.push('<?php the_sub_field('option_price') ?>');
                                                                    </script>
                                                                    <option 
                                                                        class="<?php echo $are_children ? 'child_of_' . get_sub_field('parent_code') : ''; ?>" 
                                                                        data-option-price="<?php echo $priceable ? get_sub_field('option_price') : 0; ?>"
                                                                        data-display_on="<?php the_sub_field('display_on_option_code'); ?>" 
                                                                        data-option_code="<?php the_sub_field('option_code'); ?>" 
                                                                        value="<?php the_sub_field('option'); ?>"><?php the_sub_field('option'); ?>
                                                                    </option>
                                                                    <?php
                                                                endwhile;
                                                            endif;
                                                        endwhile;
                                                    endif;
                                                    ?>
                                                </select>
                                                <?php if (get_sub_field('price_code')): ?>                                                
                                                    <input class="print_price" id="<?php the_sub_field('price_code'); ?>" type="hidden" name="pre_<?php the_sub_field('price_code'); ?>" value="0"/>
                                                    <script type="text/javascript">
                                                        jQuery(document).ready(function ($) {
                                                            $('#<?php the_sub_field('price_code'); ?>').val(slider_prices[0]);
                                                        });
                                                    </script>
                                                <?php endif; ?>
                                                <script type="text/javascript">
                                                    jQuery(document).ready(function ($) {
                                                        $('#<?php the_sub_field('code'); ?>').attr('data-price', slider_prices[0]);
                                                    });
                                                </script>
                                            </div>
                                        <?php elseif (get_row_layout() == 'checkbox'): ?> 
                                            <?php
                                            $parent_code = get_sub_field('code');
                                            ?>
                                            <div id="<?php echo $parent_code; ?>" class="form-group <?php echo get_sub_field('priceable') ? 'priceable' : ''; ?>" data-price="0" data-montage_preis="0" data-count="1">
                                                <label for="<?php the_sub_field('code'); ?>"><?php the_sub_field('name'); ?></label>
                                                <?php
                                                $priceable = get_sub_field('priceable');
                                                $montage_preis_code = get_sub_field('montage_preis_code');
                                                if (have_rows('options')):
                                                    while (have_rows('options')): the_row();
                                                        $are_children = get_sub_field('are_children');
                                                        $has_article_no = get_sub_field('has_article_no');
                                                        $checkbox_article_no_code = get_sub_field('article_no_code');
                                                        if (have_rows('option_list')):
                                                            while (have_rows('option_list')): the_row();
                                                                $content_i++;
                                                                $option_type = $priceable ? get_sub_field('option_type') : '';
                                                                ?>
                                                                <div class="checkbox">
                                                                    <label 
                                                                        class="<?php echo $are_children ? 'child_of_' . get_sub_field('parent_code') : ''; ?>" 
                                                                        data-display_on="<?php the_sub_field('display_on_option_code'); ?>"
                                                                        for="<?php echo ($parent_code . '_' . $content_i) ?>">
                                                                        <input
                                                                            class="<?php echo $priceable ? 'update_price' : ''; ?> <?php echo ($option_type == 'pm2' || $option_type == 'limit') ? 'calculate_measure_' . $option_type : ''; ?> <?php echo $has_article_no ? 'update_article_no' : ''; ?>"
                                                                            type="checkbox" 
                                                                            data-control_type="checkbox" 
                                                                            data-option_type="<?php echo $option_type; ?>" 
                                                                            id="<?php echo ($parent_code . '_' . $content_i) ?>" 
                                                                            data-option-calculate="<?php echo $priceable ? get_sub_field('option_calculate') : 'count'; ?>" 
                                                                            data-option-price="<?php echo $priceable ? get_sub_field('option_price') : 0; ?>" 
                                                                            data-option-limit="<?php echo $priceable ? get_sub_field('option_limit') : 0; ?>" 
                                                                            data-option-price_1="<?php echo $priceable ? get_sub_field('option_price_1') : 0; ?>" 
                                                                            data-option-price_2="<?php echo $priceable ? get_sub_field('option_price_2') : 0; ?>" 
                                                                            data-article_no="<?php echo get_sub_field('article_no'); ?>" 
                                                                            data-montage_preis="<?php echo $montage_preis_code ? get_sub_field('montage_preis') : 0; ?>" 
                                                                            data-option-measure="1" 
                                                                            value="<?php the_sub_field('option'); ?>" 
                                                                            name="pre_<?php echo $parent_code; ?>[]"><?php the_sub_field('option'); ?>
                                                                            <?php
                                                                            if (have_rows('individual')):
                                                                                while (have_rows('individual')): the_row();
                                                                                    ?>
                                                                                <input class="individual individual_<?php echo ($parent_code . '_' . $content_i) ?>_price" id="<?php the_sub_field('price_code'); ?>" type="hidden" name="pre_<?php the_sub_field('price_code'); ?>" value=""/>
                                                                                <input class="individual individual_<?php echo ($parent_code . '_' . $content_i) ?>_montage" id="<?php the_sub_field('montage_price_code'); ?>" type="hidden" name="pre_<?php the_sub_field('montage_price_code'); ?>" value=""/>
                                                                                <input class="individual individual_<?php echo ($parent_code . '_' . $content_i) ?>_article" id="<?php the_sub_field('article_no_code'); ?>" type="hidden" name="pre_<?php the_sub_field('article_no_code'); ?>" value=""/>
                                                                                <?php
                                                                            endwhile;
                                                                        endif;
                                                                        ?>
                                                                    </label>
                                                                </div>
                                                                <?php
                                                            endwhile;
                                                        endif;
                                                    endwhile;
                                                endif;
                                                ?>
                                                <?php if (get_sub_field('price_code')): ?>                                                
                                                    <input class="print_price" id="<?php the_sub_field('price_code'); ?>" type="hidden" name="pre_<?php the_sub_field('price_code'); ?>" value="0"/>
                                                <?php endif; ?>
                                                <?php if ($has_article_no): ?>                                                
                                                    <input class="article_no" id="<?php echo $checkbox_article_no_code; ?>" type="hidden" name="pre_<?php echo $checkbox_article_no_code; ?>" value=""/>
                                                <?php endif; ?>
                                                <?php if ($montage_preis_code): ?>                                                
                                                    <input class="preis_code" id="<?php echo $montage_preis_code; ?>" type="hidden" name="pre_<?php echo $montage_preis_code; ?>" value=""/>
                                                <?php endif; ?>
                                            </div>
                                        <?php elseif (get_row_layout() == 'radio_button'): ?> 
                                            <?php
                                            $parent_code = get_sub_field('code');
                                            ?>
                                            <div id="<?php echo $parent_code; ?>" class="form-group <?php echo get_sub_field('priceable') ? 'priceable' : ''; ?>" data-price="0" data-count="1">
                                                <label for="<?php the_sub_field('code'); ?>"><?php the_sub_field('name'); ?></label>
                                                <script type="text/javascript">
                                                    var slider_prices = [];
                                                </script>
                                                <?php
                                                $is_parent = get_sub_field('is_parent') ? 'parent' : '';
                                                $priceable = get_sub_field('priceable');
                                                if (have_rows('options')):
                                                    while (have_rows('options')): the_row();
                                                        ?>
                                                        <script type="text/javascript">
                                                            slider_prices.push('<?php echo $priceable ? get_sub_field('option_price') : 0; ?>');
                                                        </script>
                                                        <?php
                                                        $are_children = get_sub_field('are_children');
                                                        $has_article_no = get_sub_field('has_article_no');
                                                        $radio_button_article_no_code = get_sub_field('article_no_code');
                                                        if (have_rows('option_list')):
                                                            while (have_rows('option_list')): the_row();
                                                                $content_i++;
                                                                ?>
                                                                <div class="radio">
                                                                    <label 
                                                                        class="<?php echo $are_children ? 'child_of_' . get_sub_field('parent_code') : ''; ?>" 
                                                                        data-display_on="<?php the_sub_field('display_on_option_code'); ?>" 
                                                                        data-option_code="<?php the_sub_field('option_code'); ?>"
                                                                        for="<?php echo ($parent_code . '_' . $content_i) ?>">
                                                                        <input 
                                                                            class="<?php echo $is_parent; ?> <?php echo $priceable ? 'update_price' : ''; ?> <?php echo $has_article_no ? 'update_article_no' : ''; ?>" 
                                                                            data-parent_code="<?php echo $parent_code; ?>" 
                                                                            data-control_type="radio" 
                                                                            data-option-price="<?php echo $priceable ? get_sub_field('option_price') : 0; ?>"
                                                                            type="radio"  
                                                                            data-article_no="<?php echo get_sub_field('article_no'); ?>" 
                                                                            id="<?php echo ($parent_code . '_' . $content_i) ?>" 
                                                                            value="<?php the_sub_field('option'); ?>" 
                                                                            name="pre_<?php echo $parent_code; ?>">
                                                                            <?php the_sub_field('option'); ?> 
                                                                    </label>
                                                                </div>
                                                                <?php
                                                            endwhile;
                                                        endif;
                                                    endwhile;
                                                endif;
                                                ?>                                                
                                                <?php if (get_sub_field('price_code')): ?>                                                
                                                    <input class="print_price" id="<?php the_sub_field('price_code'); ?>" type="hidden" name="pre_<?php the_sub_field('price_code'); ?>" value="0"/>
                                                    <script type="text/javascript">
                                                        jQuery(document).ready(function ($) {
                                                            $('#<?php the_sub_field('price_code'); ?>').val(slider_prices[0]);
                                                        });
                                                    </script>
                                                <?php endif; ?>
                                                <?php if ($has_article_no): ?>                                                
                                                    <input class="article_no" id="<?php echo $radio_button_article_no_code; ?>" type="hidden" name="pre_<?php echo $radio_button_article_no_code; ?>" value=""/>
                                                <?php endif; ?>
                                            </div>
                                        <?php elseif (get_row_layout() == 'image_radio_button'): ?> 
                                            <?php
                                            $parent_code = get_sub_field('code');
                                            ?>
                                            <div id="<?php echo $parent_code; ?>" class="form-group <?php echo get_sub_field('priceable') ? 'priceable' : ''; ?>" data-price="0">
                                                <label for="<?php the_sub_field('code'); ?>" class="mb15"><?php the_sub_field('name'); ?></label>
                                                <script type="text/javascript">
                                                    var slider_prices = [];
                                                </script>
                                                <?php
                                                $is_parent = get_sub_field('is_parent') ? 'parent' : '';
                                                $priceable = get_sub_field('priceable');
                                                if (have_rows('options')):
                                                    while (have_rows('options')): the_row();
                                                        ?>
                                                        <script type="text/javascript">
                                                            slider_prices.push('<?php echo $priceable ? get_sub_field('option_price') : 0; ?>');
                                                        </script>
                                                        <?php
                                                        $are_children = get_sub_field('are_children');
                                                        if (have_rows('option_list')):
                                                            while (have_rows('option_list')): the_row();
                                                                $content_i++;
                                                                ?>
                                                                <div class="radio-inline pl0">
                                                                    <label 
                                                                        class="<?php echo $are_children ? 'child_of_' . get_sub_field('parent_code') : ''; ?>" 
                                                                        data-display_on="<?php the_sub_field('display_on_option_code'); ?>" 
                                                                        data-option_code="<?php the_sub_field('option_code'); ?>"
                                                                        for="<?php echo ($parent_code . '_' . $content_i) ?>">
                                                                            <?php
                                                                            $image = get_sub_field('image');
                                                                            $thumb = $image['sizes']['thumbnail'];
                                                                            if (!empty($image)):
                                                                                ?>
                                                                            <img src="<?php echo $thumb; ?>" width="75" height="75" class="img-rounded img-responsive"/>
                                                                            <?php
                                                                        endif;
                                                                        ?>
                                                                        <input 
                                                                            class="dnone <?php echo $is_parent; ?> <?php echo $priceable ? 'update_price' : ''; ?>"
                                                                            data-parent_code="<?php echo $parent_code; ?>" 
                                                                            data-control_type="radio_image" 
                                                                            data-option-price="<?php echo $priceable ? get_sub_field('option_price') : 0; ?>"
                                                                            type="radio"
                                                                            id="<?php echo ($parent_code . '_' . $content_i) ?>" 
                                                                            value="<?php the_sub_field('option'); ?>" 
                                                                            name="pre_<?php echo $parent_code; ?>"><?php the_sub_field('option'); ?>
                                                                    </label>
                                                                </div>
                                                                <?php
                                                            endwhile;
                                                        endif;
                                                    endwhile;
                                                endif;
                                                ?>
                                                <?php if (get_sub_field('price_code')): ?>                                                
                                                    <input class="print_price" id="<?php the_sub_field('price_code'); ?>" type="hidden" name="pre_<?php the_sub_field('price_code'); ?>" value="0"/>
                                                    <script type="text/javascript">
                                                        jQuery(document).ready(function ($) {
                                                            $('#<?php the_sub_field('price_code'); ?>').val(slider_prices[0]);
                                                        });
                                                    </script>
                                                <?php endif; ?>
                                                <?php if (get_sub_field('has_article_no')): ?>                                                
                                                    <input id="<?php the_sub_field('article_no_code'); ?>" type="hidden" name="pre_<?php the_sub_field('article_no_code'); ?>" value="<?php the_sub_field('article_no'); ?>"/>
                                                <?php endif; ?>
                                            </div>
                                            <script type="text/javascript">
                                                jQuery(document).ready(function ($) {
                                                    $('#' + '<?php the_sub_field('code'); ?>' + ' label').click(function () {
                                                        $(this).parent().addClass('active').siblings('.active').removeClass('active');
                                                    });
                                                });
                                            </script>
                                        <?php endif;
                                        ?>
                                    </div>
                                    <?php
                                endwhile;
                            endif;
                            ?>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
	                    <p style="font-size: 10px;"><b>Mit dem Absenden des Formulars akzeptieren Sie unsere Hinweise zur Datenspeicherung und unsere <a href="https://gewa.de/datenschutz/" target="_blank">Datenschutzerklärung</a></b>: Ich habe die Datenschutzerklärung zur Kenntnis genommen. Ich stimme zu, dass meine Angaben und Daten zur Beantwortung meiner Anfrage, Erstellung der angefragten Konfiguration bzw. eines entsprechenden Angebotes sowie Kontaktaufnahme elektronisch erhoben und gespeichert werden. Ferner bin ich damit einverstanden, dass meine Daten für eine eventuelle werbliche Ansprache verarbeitet und genutzt werden. Hinweis: Du kannst Deine Einwilligung jederzeit mit Wirkung für die Zukunft per E-Mail an info@gewa.de widerrufen.
	                    </p>
                    </div>
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
<div class="alert alert-success">
                            Sum: <p id="summarise" class="alert-link dinblock">--</p><strong> €</strong>
                        </div>
                    </div>
                    <div class="col-xs-12 text-right">
                        <button type="submit" class="btn btn-primary">Abschicken</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".update_price").change(function () {
                var control_type = $(this).data('control_type');
                data_price = 0;
                montage_prise = 0;

                if (control_type === 'select') {
                    data_price = $('option:selected', this).data('option-price');
                    $(this).attr('data-price', data_price);
                    $(this).parent().find('input.print_price').val(data_price);
                } else if (control_type === 'radio' || control_type === 'radio_image') {
                    data_price = $(this).data('option-price');
                    $(this).closest('.priceable').attr('data-price', data_price);
                    $(this).closest('.priceable').find('input.print_price').val(data_price);
                } else if (control_type === 'checkbox') {
                    $(this).closest('.priceable').find('input.individual').each(function () {
                        $(this).val('');
                    });
                    $(this).closest('.priceable').find('input:checked').each(function () {
                        option_type = $(this).data('option_type');
                        option_price = parseFloat($(this).data('option-price'));
                        option_calculate = $(this).data('option-calculate');
                        the_data_count = parseFloat($(this).closest('.priceable').attr('data-count'));
                        individual_price = 0;
                        individual_montage = 0;
                        switch (option_type) {
                            case 'pm2':
                                {
                                    option_measure = parseFloat(jQuery(this).attr('data-option-measure'));
                                    individual_price = option_calculate == 'once' ? (option_price * option_measure / the_data_count) : (option_price * option_measure);
                                    data_price += individual_price;
                                }
                                break;
                            case 'limit':
                                {
                                    option_measure = parseFloat(jQuery(this).attr('data-option-measure'));
                                    option_limit = parseFloat(jQuery(this).data('option-limit'));
                                    option_price_1 = parseFloat(jQuery(this).data('option-price_1'));
                                    option_price_2 = parseFloat(jQuery(this).data('option-price_2'));
                                    individual_price = option_calculate == 'once' ? (((option_measure <= option_limit ? option_price_1 : option_price_2)) / the_data_count) : ((option_measure <= option_limit ? option_price_1 : option_price_2));
                                    data_price += individual_price;
                                }
                                break;
                            case 'fix':
                                {
                                    individual_price = option_calculate == 'once' ? (option_price / the_data_count) : (option_price);
                                    data_price += individual_price;
                                }
                                break;
                            default:
                            {
                                individual_price = option_calculate == 'once' ? (option_price / the_data_count) : (option_price);
                                data_price += individual_price;
                            }
                        }
                        individual_montage = parseFloat($(this).data('montage_preis')) * the_data_count;
                        montage_prise += individual_montage;
                        
                        $('.individual_'+$(this).attr('id')+'_price').val((individual_price * the_data_count).toFixed(2));
                        $('.individual_'+$(this).attr('id')+'_montage').val(individual_montage.toFixed(2));
                        $('.individual_'+$(this).attr('id')+'_article').val($(this).data('article_no'));
                    });
                    $(this).closest('.priceable').attr('data-price', data_price);
                    $(this).closest('.priceable').attr('data-montage_preis', montage_prise);
                    $(this).closest('.priceable').find('.print_price').val(data_price);
                    $(this).closest('.priceable').find('.preis_code').val(montage_prise);
                }
                calculate_price();
            });
            $(".update_article_no").change(function () {
                var control_type = $(this).data('control_type');
                if (control_type === 'radio') {
                    $(this).closest('.radio').parent().find('input.article_no').val($(this).data('article_no'));
                } else if (control_type === 'checkbox') {
                    checkbox_article_no = '';
                    $(this).closest('.priceable').find('input:checked').each(function () {
                        checkbox_article_no += $(this).data('article_no') + ', ';
                    });
                    $(this).closest('.checkbox').parent().find('input.article_no').val(checkbox_article_no);
                }
            });
            $(".parent").change(function () {
                var parent_code = $(this).data('parent_code');
                var control_type = $(this).data('control_type');
                var parent_option = $(this).data('option_code');
                if (control_type == 'select') {
                    parent_option = $(this).parent().find('option:selected').data('option_code');
                }
                $('.child_of_' + parent_code).each(function () {
                    var display_on = $(this).data('display_on');
                    var display_on_arr = display_on.split(', ');
                    if (display_on_arr.indexOf(parent_option + "") > -1) {
                        $(this).addClass('to_show');
                    } else {
                        $(this).addClass('to_hide');
                    }
                });
                $('.child_of_' + parent_code).each(function () {
                    control_type = $(this).find('[data-control_type="defined_slider"]').length > 0 ? 'defined_slider' : ($(this).find('[data-control_type="select"]').length > 0 ? 'select' : ($(this).find('[data-control_type="radio"]').length > 0 || $(this).find('[data-control_type="radio_image"]').length > 0 || $(this).find('[data-control_type="checkbox"]').length > 0 ? 'radio' : ($(this).parent('[data-control_type="select"]').length > 0 ? 'select_option' : '')));
                    if (control_type === 'defined_slider') {
                        if ($(this).hasClass('to_hide')) {
                            $('select.the_type', this).val($('select.the_type', this).find("option:first").val());
                            $('select.the_breite', this).val($('select.the_breite', this).find("option:first").val());
                            $('select.the_tiefe', this).val($('select.the_tiefe', this).find("option:first").val());
                            $('select.the_hohe', this).val($('select.the_hohe', this).find("option:first").val());
                            breite_change('#' + $('select.the_breite', this).attr('id'));
                            tiefe_change('#' + $('select.the_tiefe', this).attr('id'));
                            set_price('#' + $('select.the_breite', this).attr('id'));
                            children_view($('select.the_breite', this).attr('id'), false);
                            children_view($('select.the_type', this).attr('id'), false);
                        } else {
                            set_price('#' + $('select.the_breite', this).attr('id'));
                            children_view($('select.the_breite', this).attr('id'), true);
                            children_view($('select.the_type', this).attr('id'), true);
                        }
                    }
                    if (control_type == 'select') {
                        if ($(this).hasClass('to_hide')) {
                            $('select', this).val($('select', this).find("option:first").val());
                            if ($('select', this).hasClass('counter_field')) {
                                var counter_value = $('select', this).find("option:first").val();
                                var counter_of = $('select', this).data('counter-of');
                                var counter_of_arr = counter_of.split(', ');
                                counter_of_arr.forEach(function (parent_code) {
                                    $('#' + parent_code).closest('.priceable').attr('data-count', counter_value);
                                    set_price('#' + parent_code);
                                });
                            }
                        }
                    } else if (control_type == 'select_option') {
                        if ($(this).hasClass('to_hide')) {
                            $(this).removeAttr('selected');
                            $(this).parent().val($(this).parent().find("option.to_show:not(.to_hide):first").val());
                        }
                    } else if (control_type == 'radio') {
                        if ($(this).hasClass('to_hide')) {
                            $('input', this).removeAttr('checked');
                            $(this).closest('.radio-inline').removeClass('active');
                        }
                    } else {
                        if ($(this).hasClass('to_hide')) {
                            $('input[type!=hidden]', this).val('');
                            $('textarea', this).val('');
                            var slider_min_val = $('input', this).data('slider-min');
                            if (typeof slider_min_val != 'undefined') {
                                var slider_max_val = $('input', this).data('slider-max');
                                var slider_step = $('input', this).data('my-step');
                                $('.slider', this).slider({min: slider_min_val, max: slider_max_val, step: slider_step, value: slider_min_val});
                            }
                        }
                    }
                });
                $('.to_show').attr('style', 'display: inline-block !important').removeClass('to_show').removeClass('deny_pricable');
                $('.to_hide').attr('style', 'display: none !important').removeClass('to_hide').addClass('deny_pricable');

                calculate_price();
            });
            $(".counter_field").change(function () {
                var counter_value = $(this).val();
                var counter_of = $(this).data('counter-of');
                var counter_of_arr = counter_of.split(', ');
                counter_of_arr.forEach(function (parent_code) {
                    $('#' + parent_code).closest('.priceable').attr('data-count', counter_value);
                    set_price('#' + parent_code);
                });
            });
            jQuery('.calculator_type').change(function () {
                selected_breite = parseFloat(jQuery('[data-display_on="' + jQuery(this).find('option:selected').data('option_code') + '"] .the_breite').val());
                selected_tiefe = parseFloat(jQuery('[data-display_on="' + jQuery(this).find('option:selected').data('option_code') + '"] .the_tiefe').val());
                if (isNaN(selected_breite)) {
                    var selected_breite = 0;
                }
                if (isNaN(selected_tiefe)) {
                    var selected_tiefe = 0;
                }
                jQuery('.calculate_measure_pm2').attr('data-option-measure', parseFloat(selected_breite) * parseFloat(selected_tiefe));
                jQuery('.calculate_measure_limit').attr('data-option-measure', parseFloat(selected_breite));
                update_price();
            });
            $(".parent").trigger('change');
            calculate_price();
        });
    </script>
<?php
endif;
?>