jQuery(document).ready(function ($) {
    $(".clear-list").click(function () {
        call_my_ajax($, $(this).data('configurator'), 'list', ' reset is done!');
    });
    $(".reset-counter").click(function () {
        call_my_ajax($, $(this).data('configurator'), 'counter', ' reset is done!');
    });
    $(".backup-list").click(function () {
        call_my_ajax($, $(this).data('configurator'), 'backup', ' is done!');
    });
});
function call_my_ajax($, configurator, file, my_msg) {
    var data = {
        action: 'my_action',
        security: MyAjax.security,
        configurator: configurator,
        file: file,
        mymsg: my_msg
    };

    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
    $.post(MyAjax.ajaxurl, data, function (response) {
        alert(response);
    });

}