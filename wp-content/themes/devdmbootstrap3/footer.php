<div class="dmbs-footer">
    <?php
    global $dm_settings;
    if ($dm_settings['author_credits'] != 0) :
        ?>
        <div class="row dmbs-author-credits">
            <p class="text-center"><a href="<?php
                global $developer_uri;
                echo esc_url($developer_uri);
                ?>">DevDmBootstrap3 <?php _e('created by', 'devdmbootstrap3') ?> Danny Machal</a></p>
        </div>
    <?php endif; ?>

    <?php get_template_part('template-part', 'footernav'); ?>
</div>

</div>
<!-- end main container -->
<!-- Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Login</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="front_password" class="control-label">Password:</label>
                        <input type="text" class="form-control" id="front_password" name="front_password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
<?php if (!empty($_POST) && isset($_POST['front_password']) && $_POST['front_password'] != '' && $_POST['front_password'] == 'gewa123'): ?>
    <script>
        jQuery(document).ready(function ($) {
            $.cookie("front_password", 'true', { expires: 7, path: '/' });
        });
    </script>   
<?php endif; ?>
<script>
    jQuery(document).ready(function ($) {
        var front_password = $.cookie("front_password");
        if(front_password == 'true'){
            $("#login").remove();
            $("#login-btn").remove();
        } else {
            $("#logout-btn").remove();            
            $(".download-list").remove();
            $(".clear-list").remove();
            $(".reset-counter").remove();
            $(".download_backups").remove();
            $(".backup-list").remove();
        }
        
    });
    function logoutme(){
        jQuery.removeCookie('front_password', { path: '/' });
        location.href = window.location.pathname;
    }
</script>   

</body>
</html>